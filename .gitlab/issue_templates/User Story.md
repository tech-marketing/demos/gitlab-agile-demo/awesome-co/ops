## VALUE STATEMENT

### Who is Benefiting

<!-- Example: Customers: Who will receive instant support and answers to their inquiries, improving 
their shopping experience and satisfaction. -->

- ...

### What are we building?

<!-- Example: A live chat support feature on the company's main website that allows customers to 
initiate real-time conversations with customer service representatives. -->

- ...

### Why are we building this?

<!-- Example: To Enhance Customer Support: Providing immediate assistance to help resolve issues 
and answer questions, thereby improving the overall customer experience. -->

- ....

## ACCEPTANCE CRITERIA

- [ ] Scenario 1:
  - **Given** 
  - **When** 
  - **Then**
- [ ] Scenario 2: 
  - **Given** 
  - **When** 
  - **Then**
- [ ] Scenario 3:
  - **Given** 
  - **When** 
  - **Then**

## NON-FUNCTIONAL TESTING (ileities)

- [ ] ...
- [ ] ...

## DEPENDENCIES

- ...
- ...

## DEV NOTES

- ...

## ATTACHMENT & LINKS

- [Link 1]()
- [Link 2]()

## COLLABORATION METHOD

- [ ] Pairing
- [ ] Peer code review
- [ ] ...

## PLANNING & EXECUTION  

...

## SUB-TASKS

<!-- you can convert these to first-class Tasks as soon as this issue gets created. https://docs.gitlab.com/ee/user/tasks.html#from-a-task-list-item >

- [ ] Task 1
- [ ] Task 2

/label ~"type::story" ~"wf::triage"